package ru.sinotovv.interfaces;

public interface Calculate {
    Double calculate(String task);
}
