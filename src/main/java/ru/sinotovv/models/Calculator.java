package ru.sinotovv.models;

import ru.sinotovv.interfaces.Calculate;

import java.util.Map;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Calculator implements Calculate {

    private final Stack<Double> dataStack; // Стек для хранения чисел
    private final Stack<Options> taskStack; // Стек для хранения операций
    private final Map<String, Options> vocabulary; // Словарь операций и их приоритетов

    public Calculator() {
        dataStack = new Stack<>();
        taskStack = new Stack<>();

        // Инициализация словаря операций и их приоритетов
        vocabulary = Stream.of(new Object[][]{
                {"+", new Options(6, () ->
                        dataStack.push(dataStack.pop() + dataStack.pop())
                )},
                {"-", new Options(6, () -> {
                    double tmp = dataStack.pop();
                    dataStack.push(dataStack.pop() - tmp);
                })
                },
                {"*", new Options(7, () -> dataStack.push(dataStack.pop() * dataStack.pop()))},
                {"/", new Options(7, () -> {
                    double tmp = dataStack.pop();
                    dataStack.push(dataStack.pop() / tmp);
                })},
                {"pow", new Options(8, () -> {
                    double tmp = dataStack.pop();
                    dataStack.push(Math.pow(dataStack.pop(), tmp));
                })},
                {"abs", new Options(9, () -> dataStack.push(Math.abs(dataStack.pop())))}
        }).collect(Collectors.toMap(data -> (String) data[0], data -> (Options) data[1]));
    }

    @Override
    public Double calculate(String task) {
        System.out.println(prepareString(task)); // Выводим подготовленную строку задачи
        String[] wordsTask = prepareString(task).split(" "); // Разбиваем строку задачи на отдельные слова
        for (String s : wordsTask) {
            if (vocabulary.containsKey(s)) { // Если слово содержится в словаре операций
                if (taskStack.isEmpty()) {
                    // Выполняем код операции, пока приоритет стека операций не станет ниже приоритета текущей операции
                    while (!taskStack.empty() && taskStack.peek().getPriority() >= vocabulary.get(s).getPriority()) {
                        taskStack.pop().getCode().run();
                    }
                }
                taskStack.push(vocabulary.get(s)); // Добавляем текущую операцию в стек операций
            } else if (s.equals("(")) {
                // Открывающая скобка - добавляем в стек операций с нулевым приоритетом
                taskStack.push(new Options(0, null));
            } else if (s.equals(")")) {
                Options tmp = taskStack.pop(); // Закрывающая скобка
                while (tmp.getCode() != null) {
                    tmp.getCode().run(); // Выполняем код операций до открывающей скобки
                    tmp = taskStack.pop();
                }
            } else {
                // Если слова нет в словаре операций, определяем его как число и добавляем в стек чисел
                dataStack.push(Double.valueOf(s));
            }
        }
        while (!taskStack.empty()) {
            taskStack.pop().getCode().run(); // Выполняем оставшиеся операции в стеке
        }
        return dataStack.pop(); // Возвращаем конечный результат
    }

    private String prepareString(String str) {
        // Подставляем пробелы перед и после операций и чисел
        return str.replaceAll("[*/()]|abs|pow|[-+]?(\\d+\\.\\d*|\\d*\\.\\d+)([eE][-+]?\\d+)?", " $0 ")
                .replaceAll("\\s+", " ") // Удаляем лишние пробелы
                .trim();
    }

    private static class Options {
        private final Integer priority; // Приоритет операции
        private final Runnable code; // Код операции

        public Integer getPriority() {
            return priority;
        }

        public Runnable getCode() {
            return code;
        }

        public Options(Integer priority, Runnable code) {
            this.priority = priority;
            this.code = code;
        }
    }
}